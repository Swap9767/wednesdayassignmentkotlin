package com.assignmentwednesdaykt.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.assignmentwednesdaykt.R
import com.assignmentwednesdaykt.databinding.ItunesListViewBinding
import com.assignmentwednesdaykt.model.database.ITunesTable
import com.squareup.picasso.Picasso

public class ShowTunesAdapter(
    private val mContext: Context,
    private val iTunesList: List<ITunesTable>
) :
    BaseAdapter() {
    private val mInflater: LayoutInflater = LayoutInflater.from(mContext)

    override fun getCount(): Int {
        return iTunesList.size
    }

    override fun getItem(position: Int): Any {
        return iTunesList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(
        position: Int,
        view: View?,
        parent: ViewGroup
    ): View {
        var iTunesListViewBinding: ItunesListViewBinding =
            DataBindingUtil.inflate(mInflater, R.layout.itunes_list_view, parent, false)
        Picasso.get().load(iTunesList[position].image)
            .into(iTunesListViewBinding.ivITunes)
        (iTunesListViewBinding.tvArtistName).text = iTunesList[position].artistName
        (iTunesListViewBinding.tvTrackName).text = iTunesList[position].trackName
        return iTunesListViewBinding.root
    }
}
