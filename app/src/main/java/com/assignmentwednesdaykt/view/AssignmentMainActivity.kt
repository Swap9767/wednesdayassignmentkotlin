package com.assignmentwednesdaykt.view

import android.app.SearchManager
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.MenuItemCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.assignmentwednesdaykt.R
import com.assignmentwednesdaykt.databinding.ActivityAssignmentMainBinding
import com.assignmentwednesdaykt.model.database.ITunesTable
import com.assignmentwednesdaykt.model.network.ITunesPojo
import com.assignmentwednesdaykt.viewModel.AssignmentViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class AssignmentMainActivity : AppCompatActivity() {

    lateinit var assignmentViewModel: AssignmentViewModel
    lateinit var activityAssignmentBinding: ActivityAssignmentMainBinding
    var lastSearchArtist = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityAssignmentBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_assignment_main)
        assignmentViewModel = ViewModelProviders.of(this).get(AssignmentViewModel::class.java)
    }

    private fun loadFromDb(searchString: String) {
        var iTunesList: ArrayList<ITunesTable> = ArrayList<ITunesTable>()

        CoroutineScope(IO).launch {
            iTunesList = assignmentViewModel.getSelectedArtistInfo(
                searchString.trim()
            ) as ArrayList<ITunesTable>


            withContext(Main) {
                try {
                    activityAssignmentBinding.pbLoad.visibility = (View.GONE)
                    if (iTunesList.isEmpty()) {
                        Toast.makeText(
                            applicationContext,
                            "Check Your Internet Connectivity",
                            Toast.LENGTH_LONG
                        ).show()
                        return@withContext
                    }
                    setAdapter(iTunesList)
                    activityAssignmentBinding.gvITunesList.visibility = (View.VISIBLE)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            lastSearchArtist =
                searchString.trim()
        }
    }

    private fun loadRequest(search: String) {
        var iTunesList: ArrayList<ITunesTable> = ArrayList<ITunesTable>()
        assignmentViewModel.getITunesData(
            search
        )?.observe(
            this@AssignmentMainActivity, androidx.lifecycle.Observer { t ->
                if (t.resultList.isEmpty()) {
                    Toast.makeText(applicationContext, "list is empty", Toast.LENGTH_LONG).show()
                }
                t.resultList.forEach {
                    Log.d("Log -- > ", " track ${it.trackName}")
                    val iTunesTable = ITunesTable(
                        0,
                        it.image,
                        it.trackName,
                        it.artistName,
                        search.trim()
                    )
                    CoroutineScope(IO).launch {
                        assignmentViewModel.insertITunes(
                            iTunesTable
                        )
                    }
                    iTunesList.add(iTunesTable)
                }
                setAdapter(iTunesList)
                activityAssignmentBinding.pbLoad.visibility = (View.GONE)
                activityAssignmentBinding.gvITunesList.visibility =
                    (View.VISIBLE)
            })
    }

    private fun setAdapter(iTunesList: ArrayList<ITunesTable>) {
        activityAssignmentBinding.gvITunesList.adapter = ShowTunesAdapter(this, iTunesList)
    }

    private fun checkNetwork(): Boolean {
        val conMgr =
            (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        val netInfo = conMgr.activeNetworkInfo
        return netInfo != null
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchItem: MenuItem = menu!!.findItem(R.id.action_search)

        if (searchItem != null) {
            var searchView = MenuItemCompat.getActionView(searchItem) as SearchView
            searchView.setOnCloseListener { true }

            val searchPlate =
                searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
            searchPlate.hint = "Search Artist"
            val searchPlateView: View =
                searchView.findViewById(androidx.appcompat.R.id.search_plate)
            searchPlateView.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    android.R.color.transparent
                )
            )

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (query != null) {
                        searchItem(query)
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })
            val searchManager =
                getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return super.onCreateOptionsMenu(menu)
    }


    private fun searchItem(searchString: String) {
        var iTunesList: ArrayList<ITunesTable> = ArrayList<ITunesTable>()

        if (TextUtils.isEmpty(searchString)) {
            Toast.makeText(this, "Enter Artist Name", Toast.LENGTH_LONG).show()
            return
        }
        if (searchString.trim().length < 3) {
            Toast.makeText(this, "Enter minimum 3 Characters", Toast.LENGTH_LONG).show()
            return
        }

        if (searchString.trim() == lastSearchArtist) {
            Toast.makeText(this, "Enter Different Artist", Toast.LENGTH_LONG).show()
            return
        }
        activityAssignmentBinding.pbLoad.visibility = (View.VISIBLE)
        activityAssignmentBinding.gvITunesList.visibility = (View.GONE)
        var isNetworkAvailable = checkNetwork()

        CoroutineScope(IO).launch {
            iTunesList = assignmentViewModel.getSelectedArtistInfo(
                searchString.trim()
            ) as ArrayList<ITunesTable>

            lastSearchArtist =
                searchString.trim()
            if (iTunesList.isEmpty()) {
                if (isNetworkAvailable) {
                    withContext(Main) {
                        loadRequest(searchString)
                    }
                } else {
                    loadFromDb(searchString)
                }
            } else {
                withContext(Main) {
                    try {
                        setAdapter(iTunesList)
                        activityAssignmentBinding.pbLoad.visibility = (View.GONE)
                        activityAssignmentBinding.gvITunesList.visibility = (View.VISIBLE)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}
