package com.assignmentwednesdaykt.viewModel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.assignmentwednesdaykt.model.database.ITunesDao
import com.assignmentwednesdaykt.model.database.ITunesRepository
import com.assignmentwednesdaykt.model.database.ITunesRoomDatabase
import com.assignmentwednesdaykt.model.database.ITunesTable
import com.assignmentwednesdaykt.model.network.ITunesDataRepository
import com.assignmentwednesdaykt.model.network.ITunesPojo
import com.assignmentwednesdaykt.model.network.ResultList

public class AssignmentViewModel(
    private val context: Application
) : AndroidViewModel(context) {

    private val iTunesDataRepository: ITunesDataRepository

    private val iTunesDBRepository: ITunesRepository

    private val iTunesDao: ITunesDao

    init {
        iTunesDao = ITunesRoomDatabase.getDatabase(context).iTunesDao()
        iTunesDBRepository = ITunesRepository(iTunesDao)
        iTunesDataRepository = ITunesDataRepository()
    }

    fun getITunesData(search: String): LiveData<ResultList>? {
        return iTunesDataRepository.getITunesTask(search)
    }

    suspend fun insertITunes(iTunesTable: ITunesTable): Long? {
        return iTunesDBRepository.insert(iTunesTable)
    }

    suspend fun getSelectedArtistInfo(search: String): List<ITunesTable>? {
        return iTunesDBRepository.getSelectedArtistInfo(search)
    }
}