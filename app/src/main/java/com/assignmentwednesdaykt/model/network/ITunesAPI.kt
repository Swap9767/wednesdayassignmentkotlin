package com.assignmentwednesdaykt.model.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


public interface ITunesAPI {
    @GET("search?")
    public fun getITunesTask(@Query("term") search: String): Call<ResultList>

}