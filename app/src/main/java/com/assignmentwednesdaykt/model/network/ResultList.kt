package com.assignmentwednesdaykt.model.network

import com.google.gson.annotations.SerializedName

class ResultList(
    @SerializedName("results")
    val resultList: ArrayList<ITunesPojo>
)