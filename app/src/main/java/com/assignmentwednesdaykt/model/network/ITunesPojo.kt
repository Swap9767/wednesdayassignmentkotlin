package com.assignmentwednesdaykt.model.network

import com.google.gson.annotations.SerializedName

data class ITunesPojo(
    @SerializedName("artworkUrl100")
    val image: String?,
    @SerializedName("trackName")
    val trackName: String?,
    @SerializedName("artistName")
    val artistName: String?
)