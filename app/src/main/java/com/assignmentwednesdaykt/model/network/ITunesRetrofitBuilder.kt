package com.assignmentwednesdaykt.model.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ITunesRetrofitBuilder {
    private const val BASE_URL = "https://itunes.apple.com/"

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient().newBuilder().connectTimeout(30, TimeUnit.SECONDS).build())
            .build()
    }

    fun <S> createService(serviceClass: Class<S>?): S {
        return getRetrofit().create(serviceClass)
    }
}