package com.assignmentwednesdaykt.model.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

public class ITunesDataRepository {
    private var iTunesAPI: ITunesAPI? = null

    init {
        iTunesAPI = ITunesRetrofitBuilder.createService(ITunesAPI::class.java)
    }

    fun getITunesTask(search: String): MutableLiveData<ResultList> {
        val iTunesData = MutableLiveData<ResultList>()

        iTunesAPI?.getITunesTask(search)?.enqueue(object : Callback<ResultList> {
            override fun onResponse(
                call: Call<ResultList>,
                response: Response<ResultList>
            ) {
                Log.d("Log -- > ", " response ${response.body()}")
                if (response.isSuccessful) {
                    iTunesData.value = response.body()
                }
            }

            override fun onFailure(call: Call<ResultList>, t: Throwable) {
                Log.e("Log -- ", "Error " + t.message)
                t.printStackTrace()
                iTunesData.value = null
            }
        })
        return iTunesData
    }
}