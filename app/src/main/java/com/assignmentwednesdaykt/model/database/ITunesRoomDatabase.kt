package com.assignmentwednesdaykt.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = arrayOf(ITunesTable::class),
    version = 1,
    exportSchema = false
)
public abstract class ITunesRoomDatabase : RoomDatabase() {

    abstract fun iTunesDao(): ITunesDao

    companion object {

        @Volatile
        private var INSTANCE: ITunesRoomDatabase? = null

        fun getDatabase(context: Context): ITunesRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ITunesRoomDatabase::class.java,
                    "iTunesDatabase"
                ).build();
                INSTANCE = instance
                return instance
            }
        }
    }
}