package com.assignmentwednesdaykt.model.database

class ITunesRepository(private val iTunesDao: ITunesDao) {

    suspend fun getSelectedArtistInfo(searchArtist: String): List<ITunesTable>? {
        return iTunesDao.getITunesDetails(searchArtist)
    }

    suspend fun insert(iTunesTable: ITunesTable): Long? {
        return iTunesDao.insert(iTunesTable)
    }
}