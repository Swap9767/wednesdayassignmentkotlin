package com.assignmentwednesdaykt.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ITunesTable")
data class ITunesTable(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "image") var image: String?,
    @ColumnInfo(name = "trackName") var trackName: String?,
    @ColumnInfo(name = "artistName") var artistName: String?,
    @ColumnInfo(name = "searchedArtists") var searchedArtists: String?
)