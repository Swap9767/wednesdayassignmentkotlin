package com.assignmentwednesdaykt.model.database

import androidx.room.*

@Dao
interface ITunesDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(iTunesTable: ITunesTable): Long?

    @Query("SELECT * FROM ITunesTable WHERE searchedArtists LIKE :search")
    fun getITunesDetails(search: String): List<ITunesTable>?

}